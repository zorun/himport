
# Usage from repository

**Installation**

    git clone https://code.ffdn.org/ffdn/himport.git
    cd himport

    virtualenv venv
    ./venv/bin pip install -e .

**Utilisation**

    ./venv/bin/himport

# Configuration

**General**

You can copy the configuration file from ``himport.conf.template``

    cp himport.conf.template himport.conf

**Local configuration**

    cp himport.conf.local.template himport.conf.local

This file is used to store SQL information to connect to the dolibarr database


# Authors

Philippe Le Brouster <plb@nebkha.net>
