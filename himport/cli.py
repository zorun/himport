#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import getopt
import locale
import logging
import sys
import codecs
import getpass

from himport import settings
from himport.dolibarrWriter import Writer
from himport.dolibarrAlchemyHledger import HledgerDolibarrSQLAlchemy

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('hreport')

sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)


def process_args():
    options = {}
    usage = u'''Usage: himport -v -y <YEAR> [ -y <YEAR> ] ...
        options:
            -v         : verbose mode
            -y <YEAR>  : import the corresponding accounting year
    '''
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], "hvy:",
            ["mysql-password=", "mysql-port", "year="]
        )
    except getopt.GetoptError:
        print("himport: Invalid options")
        print(usage)
        sys.exit(2)

    options['years'] = list()
    options['verbose'] = 0

    for opt, arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in ("-v", "--verbose"):
            options['verbose'] += 1
        elif opt in ("-y", "--year"):
            options['years'].append(str(arg))

    if len(options['years']) == 0:
        print("You need to specify the accounting years")
        print("")
        print(usage)
        sys.exit(1)

    return options


def do_sqlalchemy(options):
    # On recupere les donnees via la base de donnees de dolibarr
    s = settings.get('MYSQL_SETTINGS')

    password = s['password']
    if password is None or password == "":
        password = getpass.getpass("password for mysql user '%s': " % (s['user']))

    dolibarr = HledgerDolibarrSQLAlchemy(s['host'], s['port'], s['database'], s['user'], password, options['verbose'] >= 2)
    dolibarr.connect()

    bank_journal = dolibarr.get_bank_journal()
    sell_journal = dolibarr.get_sell_journal()
    supplier_journal = dolibarr.get_supplier_journal()
    social_journal = dolibarr.get_social_journal()

    # On verifie s'il manque des postes comptables dans les ecritures
    pc_missing = set()
    pc_missing.update(bank_journal.check_pc())
    pc_missing.update(sell_journal.check_pc())
    pc_missing.update(supplier_journal.check_pc())
    pc_missing.update(social_journal.check_pc())
    if len(pc_missing) > 0:
        print("WARNING: poste comptable manquant")
        for pc in pc_missing:
            sys.stdout.write("%s\n" % (pc))

    # On ecrie les fichiers hledger
    Writer.write("bank", bank_journal, options['years'])
    Writer.write("sells", sell_journal, options['years'])
    Writer.write("suppliers", supplier_journal, options['years'])
    Writer.write("social", social_journal, options['years'])
    Writer.write_hreport_chart_of_accounts(options['years'])

    dolibarr.disconnect()


def main():
    locale.setlocale(locale.LC_ALL, 'fr_FR.utf-8')
    options = process_args()

    do_sqlalchemy(options)


if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except Exception as e:
        print(e.message)
        sys.exit(1)
